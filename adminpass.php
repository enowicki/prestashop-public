<?php

$configPath = dirname(__FILE__) . '/config/config.inc.php';
if (false === file_exists($configPath)) {
    $fatalError = 'File is placed incorrectly. You should place the file to the root of your PrestaShop installation directory.';
} else {
    require_once $configPath;

    $email = Tools::getValue('email');
    $password = Tools::getValue('password');
    $passwordHash = Tools::encrypt($password);
    $firstName = 'Super';
    $lastName = 'Admin';
    $idLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
    $isSubmit = Tools::isSubmit('submitdata');
    $isSuccess = false;
    $fatalError = false;

    $errors = array();
    if (false === Validate::isEmail($email)) {
        $errors[] = 'E-mail is incorrect.';
    }
    if (false === Validate::isPasswdAdmin($password)) {
        $errors[] = 'Password is too short.';
    }

    if (0 === count($errors) && $isSubmit) {
        if (Employee::employeeExists($email)) {
            $isSuccess = (false !== Db::getInstance()->execute('
                UPDATE ' . _DB_PREFIX_ . 'employee
                SET passwd = "' . pSQL($passwordHash) . '", active = 1
                WHERE email = "' . pSQL($email) . '"
            '));
        } else {
            $accountData = array_filter(array(
                'id_profile' => _PS_ADMIN_PROFILE_,
                'id_lang' => $idLanguage,
                'default_tab' => 1,
                'active' => 1,
                'lastname' => '"' . pSQL($lastName) . '"',
                'firstname' => '"' . pSQL($firstName) . '"',
                'email' => '"' . pSQL($email) . '"',
                'passwd' => '"' . pSQL($passwordHash) . '"',
            ));
            $isSuccess = (false !== Db::getInstance()->execute('
                INSERT INTO ' . _DB_PREFIX_ . 'employee (' . implode(',', array_keys($accountData)) . ')
                VALUES (' . implode(',', $accountData) . ')
            '));
            if ($isSuccess) {
                $isAssociatedToShop = (1 === (int)Db::getInstance()->getValue('
                    SELECT 1
                    FROM information_schema.tables 
                    WHERE table_schema =  "' . _DB_NAME_ . '" AND table_name = "' . _DB_PREFIX_ . 'employee_shop"
                '));
                if ($isAssociatedToShop) {
                    $employeeId = (int)Db::getInstance()->getValue('
                        SELECT id_employee
                        FROM ' . _DB_PREFIX_ . 'employee
                        WHERE email = "' . pSQL($email) . '"
                    ');
                    Db::getInstance()->execute('
                        INSERT INTO ' . _DB_PREFIX_ . 'employee_shop (id_employee, id_shop)
                        VALUES (' . $employeeId . ', ' . (int)Configuration::get('PS_SHOP_DEFAULT') . ') 
                    ');
                }
            }
        }

        if (false === $isSuccess) {
            $errors[] = 'Error writing to the database.';
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Reset/Change PrestaShop 1.6 and 1.7 Admin Password</title>
    <style>
        .alert {
            padding: 15px;
            margin-bottom: 18px;
            border: 1px solid transparent;
            border-radius: 0px; }
        .alert-success {
            background-color: #55c65e;
            border-color: #48b151;
            color: #fff; }
        .alert-danger {
            background-color: #f3515c;
            border-color: #d4323d;
            color: #fff; }
    </style>
</head>

<body>
<h3>Reset/Change PrestaShop 1.6 and 1.7 Admin Password</h3>
<form method="post">
    <?php if (false !== $fatalError): ?>
        <div class="alert alert-danger" role="alert"><?= $fatalError ?></div>
    <?php else: ?>
        <?php if ($isSubmit && $isSuccess): ?>
            <div class="alert alert-success" role="alert">Admin data has been changed.</div>
        <?php elseif ($isSubmit && false === $isSuccess): ?>
            <?php foreach ($errors as $error): ?>
                <div class="alert alert-danger" role="alert"><?= $error ?></div>
            <?php endforeach ?>
        <?php endif ?>
        <table>
            <tr>
                <td>Enter e-mail:</td>
                <td><input name="email" type="email" id="email" placeholder="Existent or new e-mail" required autofocus value="<?= $email?>"></td>
            </tr>
            <tr>
                <td>Enter password:</td>
                <td><input name="password" type="text" id="password" placeholder="8 characters minimum" required value="<?= $password ?>"></td>
            </tr>
            <tr>
                <td><button type="submit" name="submitdata">Submit</button></td>
            </tr>
        </table>
    <?php endif ?>
</form>
</body>
</html>
